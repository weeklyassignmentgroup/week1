﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1.Monster
{
  public class Monster
  {
    public MonsterType Type { get; set; }
    public int Level { get; set; }
    public double Health { get; set; }
    public double Damage { get; set; }


    public Monster(MonsterType monsterType = MonsterType.Dragon, int level = 1, double health = 1, double damage = 1)
    {
      Type = monsterType;
      Level = level;
      Health = health;
      Damage = damage;
    }

    /// <summary>
    /// Monster attacks if it still lives
    /// If it is dead it does not harm anyone
    /// </summary>
    /// <returns>The monsters attackDamage</returns>
    public double AttackDamageIfAlive()
    {
      if (IsAlive())
        return Damage;
      else return 0;
    }
    /// <summary>
    /// The monster takes damage
    /// </summary>
    /// <param name="damage">The damage dealt to the monster</param>
    /// <returns>if the monster is alive after taking damage</returns>
    public bool TakeDamageReturnIsAlive(double damage)
    {
      Health -= damage;
      return IsAlive();
    }

    public bool IsAlive()
    {
      return Health > 0;
    }




  }
}
