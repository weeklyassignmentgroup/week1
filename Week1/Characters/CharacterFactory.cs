﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1.Characters
{
  /// <summary>
  /// Creates characters for use both in the RPG and for testing
  /// </summary>
  public class CharacterFactory
  {
    /// <summary>
    /// Can be used to make a default character from CharacterType
    /// These kinds of factory methods(and more advanced ones) are used for dependency reasons
    /// If you use this method you do not have to depend on Mage, Rogue etc
    /// </summary>
    /// <param name="characterType"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static Character GetCharacter(CharacterType characterType, string Name = "test")
    {
      switch (characterType)
      {
        case (CharacterType.Mage):{ 
          return GetMage(Name);
        }
        case (CharacterType.Rogue):{
            return GetRogue(Name);
        }
        case (CharacterType.Warrior):
        {
          return GetWarrior(Name);
        }
        case (CharacterType.Ranger):{
          return GetRanger(Name);
        }
        default: {
          throw new ArgumentException("Unsupported Charactertype");
        }
      }
    }

    public static Mage GetMage(string Name = "test")
    {
      return new Mage(Name);
    }

    public static Warrior GetWarrior(string Name = "test")
    {
      return new Warrior("test");
    }

    public static Rogue GetRogue(string Name = "test")
    {
      return new Rogue("test");
    }

    public static Ranger GetRanger(string Name = "test")
    {
      return new Ranger("test");
    }

  }
}
