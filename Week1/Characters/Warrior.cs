﻿
/// <summary>
/// The warrior class of Character, Strong and Heavy
/// </summary>
public class Warrior : Character
{
  public override Dictionary<ItemSlot, Item> Equipment { get; protected set; }
  public override List<WeaponType> AllowedWeaponTypes { get; protected set; }
  public override List<ArmorType> AllowedArmorTypes { get; protected set; }
  public override Dictionary<AttributeType, PrimaryAttribute> BasePrimaryAttributes { get; protected set; }
  public override Dictionary<AttributeType, PrimaryAttribute> TotalPrimaryAttributes { get; protected set; }
  public override Dictionary<AttributeType, int> AttributeGain { get; protected set; }
  public override AttributeType MyPrimaryAttribute { get; protected set; }
  /// <summary>
  /// Intitializes Warrior attributes, their gains on levelup and 
  /// wich weapon and armortypes they are allowed to equip
  /// </summary>
  /// <param name="Name"></param>
  public Warrior(string Name) : base(Name)
  {
    int InitialInteligence = 1;
    int InitialDexterity = 2;
    int InitialStrength = 5;

    MyPrimaryAttribute = AttributeType.Strength;

    TotalPrimaryAttributes = new()
    {
      {AttributeType.Intelligence, new Intelligence(InitialInteligence) },
      {AttributeType.Dexterity, new Dexterity(InitialDexterity) },
      {AttributeType.Strength, new Strength(InitialStrength) }
    };

    AttributeGain = new()
    {
      { AttributeType.Intelligence, 1 },
      { AttributeType.Dexterity, 2 },
      { AttributeType.Strength, 3 }
    };

    BasePrimaryAttributes = new()
    {
      {AttributeType.Intelligence, new Intelligence(InitialInteligence) },
      {AttributeType.Dexterity, new Dexterity(InitialDexterity) },
      {AttributeType.Strength, new Strength(InitialStrength) }
    };

    Equipment = Character.GetDummyInventoryOfItems();
    AllowedWeaponTypes = new() { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword };
    AllowedArmorTypes = new() { ArmorType.Mail, ArmorType.Plate };
  }


}