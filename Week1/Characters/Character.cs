﻿using System;
using System.Text;
using Week1.Items;

/// <summary>
/// Playable named character, has primarystats and can wield items.
/// Items and stats are combined to calculate the damage it does
/// This character can level up to gain stats
/// </summary>
public abstract class Character
{
  ///<summary>
  ///Equipment represents all the items a character currently wields
  /// </summary>
  public abstract Dictionary<ItemSlot, Item> Equipment { get; protected set; } 
  /// <summary>
  /// Defines the weapon types this type of chracter can use
  /// </summary>
  public abstract List<WeaponType> AllowedWeaponTypes { get; protected set; } 
  /// <summary>
  /// Defines the armor types this character can wield
  /// </summary>
  public abstract List<ArmorType> AllowedArmorTypes { get; protected set; }
  /// <summary>
  /// Stores the attributes this character got from leveling up
  /// </summary>
  public abstract Dictionary<AttributeType, PrimaryAttribute> BasePrimaryAttributes { get; protected set; }
  /// <summary>
  ///  Stores the attributes this character got from both leveling up and wielding items
  /// </summary>
  public abstract Dictionary<AttributeType, PrimaryAttribute> TotalPrimaryAttributes { get; protected set; }
  /// <summary>
  /// Defines how many attribtues of each type this character gets on levelup.
  /// </summary>
  public abstract Dictionary<AttributeType, int> AttributeGain { get; protected set; }
  /// <summary>
  ///  Defines the primary attribute of the character, this attributes gives the character bonus damage
  /// </summary>
  public abstract AttributeType MyPrimaryAttribute { get; protected set; }

  /// <summary>
  /// Characters are defaulted to level 1 on creation
  /// </summary>
  public int Level { get; private set; } = 1;

  /// <summary>
  /// The characters name
  /// </summary>
  public string Name { get; private set; }

  /// <summary>
  /// You cannot gain life, only loose it
  /// You start at 10 life and die if it goes zero or below
  /// </summary>
  public double Health { get; set; } = 10;

  /// <summary>
  /// This is the base class constructor
  /// More detailed constructors are made inside each childclass
  /// </summary>
  /// <param name="Name">The name you want to give the character</param>
  public Character(string Name)
  {
    this.Name = Name;


  }

  public bool IsAlive()
  {
      return Health > 0;
  }

  /// <summary>
  /// Calculates the damage the hero will do according to this formula:
  /// (WeaponDamage * WeaponAttackSpeed) * (1 + PrimaryAttribute/100)
  /// Meaning that every point of Intelligence gives 1 % more damage.
  /// </summary>
  /// <returns>calculated damage</returns>
  public double CalculateDamage()
  {

    if (Equipment[ItemSlot.Weapon].GetType() == typeof(Weapon))
    {
      Weapon Weapon = (Weapon)Equipment[ItemSlot.Weapon];
      double BaseDps = (Weapon.Damage * Weapon.AttacksPerSecound);
      double TotalDps = BaseDps * (1.0 + (TotalPrimaryAttributes[MyPrimaryAttribute].Amount / 100.0));
      return TotalDps;
    }
    else
    {
      return 1 * (1.0 + (TotalPrimaryAttributes[AttributeType.Strength].Amount / 100.0));

    }
  }
  /// <summary>
  /// Increases the stats of the character on levelup according to the statgain it has defined.
  /// </summary>
  public void LevelUp()
  {
    Level++;

    foreach (AttributeType AtrType in AttributeGain.Keys)
    {
      BasePrimaryAttributes[AtrType].AddAttribute(AttributeGain[AtrType]);
      TotalPrimaryAttributes[AtrType].AddAttribute(AttributeGain[AtrType]);


    }
  }

  /// <summary>
  /// A character tries to equip an item, that is add it to his Equipment
  /// Items require a certain level and characters are limited by what types of item they can wield.
  /// If it is unable to equip a weapon it throws InvalidWeaponException
  /// If it is unable to equip an armor it throws InvalidArmorException
  /// </summary>
  /// <param name="equipment">the item you wish to equip</param>
  /// <returns></returns>
  /// <exception cref="InvalidWeaponException">When the class cannot equip a weapon of that type</exception>
  /// <exception cref="InvalidArmorException">When the class cannot equip armor of that type</exception>
  public string Equip(Item equipment)
  {
    RequiredLevelCheck(equipment);

    if (equipment.GetType() == typeof(Weapon))
    {
      Weapon attemptedEquip = (Weapon)equipment;
      return EquipWeapon(attemptedEquip);
    }
    else if (equipment.GetType() == typeof(Armor))
    {
      Armor attemptedArmorEquip = (Armor)equipment;
      return EquipArmor(attemptedArmorEquip);
    }
    return "";
  }

  /// <summary>
  /// Checks if the character has the level required to use this item
  /// </summary>
  /// <param name="equipment"></param>
  /// <exception cref="InvalidItemException">When the character is to low lvl for the item</exception>
  private void RequiredLevelCheck(Item equipment)
  {
    if (equipment.RequiredLevel > Level)
        throw new InvalidItemException("Item requires a higher level to equip");
  }

  /// <summary>
  /// EquipWeapon sub-method of equip
  /// </summary>
  /// <param name="equipment"></param>
  /// <exception cref="InvalidWeaponException">When the class cannot equip a weapon of that type</exception>
  private string EquipWeapon(Weapon attemptedEquip)
  {
    if (AllowedWeaponTypes.Contains(attemptedEquip.Type))
    {
      Console.WriteLine("You Equip a weapon described as \n" + attemptedEquip.ToString());
      Equipment[attemptedEquip.UsedItemslot] = attemptedEquip;
      return "New weapon equipped!";
    }
    else
    {
      throw new InvalidWeaponException($"{this.GetType().Name} cannot equip {attemptedEquip.Type}");
    }
  }

  /// <summary>
  /// EquipArmor sub-method of equip
  /// </summary>
  /// <param name="attemptedEquip"></param>
  /// <returns></returns>
  /// <exception cref="InvalidArmorException">When the class cannot equip a armor of that type</exception>
  private string EquipArmor(Armor attemptedEquip)
  {
    if (AllowedArmorTypes.Contains(attemptedEquip.Type))
    {
      Console.WriteLine("You Equip armor described as \n" + attemptedEquip.ToString());

      UnEquip(attemptedEquip.UsedItemslot);
      Equipment[attemptedEquip.UsedItemslot] = attemptedEquip;
      foreach (AttributeType attributeType in attemptedEquip.AttributeBonus.Keys)
      {
        TotalPrimaryAttributes[attributeType].AddAttribute(attemptedEquip.AttributeBonus[attributeType]);
      }

      return "New Armor equipped!";

    }
    else
    {
      throw new InvalidArmorException($"{this.GetType().Name} cannot equip {attemptedEquip.Type}");
    }
  }
  /// <summary>
  /// UnEquips an item, meaning it reverses the effects the item had on our character
  /// And replaces it with a dummyItem which I use to represent Empty slot
  /// </summary>
  /// <param name="itemSlot">Itemslot you wish to unequip from</param>
  private void UnEquip(ItemSlot itemSlot)
  {
    Item equipment = Equipment[itemSlot];
    if (equipment.GetType() == typeof(Armor)){
      Armor armor = (Armor)equipment;
      foreach (AttributeType attributeType in armor.AttributeBonus.Keys)
      {
        TotalPrimaryAttributes[attributeType].SubtractAttribute(armor.AttributeBonus[attributeType].Amount);
      }
    }
    Equipment[itemSlot] = new DummyItem();
  }

  /// <summary>
  /// Determines if the character is able to equip a given item
  /// </summary>
  /// <param name="item">item you wish to know if you can equip</param>
  /// <returns>true if you can equip the item, false if not</returns>
  public bool CanEquip(Item item)
  {
    if (item.RequiredLevel > Level)
      return false;

    if (item.GetType() == typeof(Armor))
    {
      Armor armor = (Armor)item;
      return AllowedArmorTypes.Contains(armor.Type);
    }
    else if (item.GetType() == typeof(Weapon))
    {
      Weapon weapon = (Weapon)item;
      return AllowedWeaponTypes.Contains(weapon.Type);
    }
    else { 
      return false;
    }
  }


  /// <summary>
  /// This method is used to print the summary charactersheet
  /// </summary>
  /// <returns>string of the charactersheet</returns>
  public override string ToString()
  {
    StringBuilder CharSheet = new StringBuilder();

    CharSheet.Append($"This one is named {Name}, they have level {Level}\n");

    foreach (AttributeType atrType in TotalPrimaryAttributes.Keys)
    {
      CharSheet.AppendLine($"{atrType}: {TotalPrimaryAttributes[atrType].Amount}");
    }
    CharSheet.AppendLine($"Damage:"+ Math.Round(this.CalculateDamage()));

    return CharSheet.ToString();
  }

  /// <summary>
  /// Creates a full equipment set full of dummy items
  /// These you can think of as representing an empty itemslot
  /// They do not affect the character in any way
  /// </summary>
  /// <returns>a dictionary of dummy items, representing empty item slots</returns>
  public static Dictionary<ItemSlot, Item> GetDummyInventoryOfItems()
  {
    Dictionary<ItemSlot, Item> dummyItems = new Dictionary<ItemSlot, Item>();
    Array itemSlots = Enum.GetValues(typeof(ItemSlot));
    foreach (ItemSlot s in itemSlots) {
      dummyItems.Add(s, new DummyItem());
    }

    return dummyItems;
  }

}
