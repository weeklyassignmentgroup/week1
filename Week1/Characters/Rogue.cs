﻿
public class Rogue : Character
{
  public override Dictionary<ItemSlot, Item> Equipment { get; protected set; }
  public override List<WeaponType> AllowedWeaponTypes { get; protected set; }
  public override List<ArmorType> AllowedArmorTypes { get; protected set; }
  public override Dictionary<AttributeType, PrimaryAttribute> BasePrimaryAttributes { get; protected set; }
  public override Dictionary<AttributeType, PrimaryAttribute> TotalPrimaryAttributes { get; protected set; }
  public override Dictionary<AttributeType, int> AttributeGain { get; protected set; }
  public override AttributeType MyPrimaryAttribute { get; protected set; }
  /// <summary>
  /// Intitializes Rogue attributes, their gains on levelup and 
  /// wich weapon and armortypes they are allowed to equip
  /// Uses the base class to set name
  /// </summary>
  public Rogue(string Name) : base(Name)
  {
    int InitialInteligence = 1;
    int InitialDexterity = 6;
    int InitialStrength = 2;

    MyPrimaryAttribute = AttributeType.Dexterity;

    TotalPrimaryAttributes = new()
    {
      {AttributeType.Intelligence, new Intelligence(InitialInteligence) },
      {AttributeType.Dexterity, new Dexterity(InitialDexterity) },
      {AttributeType.Strength, new Strength(InitialStrength) }
    };

    AttributeGain = new()
    {
      { AttributeType.Intelligence, 1 },
      { AttributeType.Dexterity, 4 },
      { AttributeType.Strength, 1 }
    };

    BasePrimaryAttributes = new()
    {
      {AttributeType.Intelligence, new Intelligence(InitialInteligence) },
      {AttributeType.Dexterity, new Dexterity(InitialDexterity) },
      {AttributeType.Strength, new Strength(InitialStrength) }
    };

    Equipment = Character.GetDummyInventoryOfItems();
    AllowedWeaponTypes = new() { WeaponType.Sword, WeaponType.Dagger };
    AllowedArmorTypes = new() { ArmorType.Leather, ArmorType.Mail };
  }


}