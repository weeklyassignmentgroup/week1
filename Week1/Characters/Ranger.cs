﻿
public class Ranger : Character
{
  //Attributes
  public override Dictionary<ItemSlot, Item> Equipment { get; protected set; }
  public override List<WeaponType> AllowedWeaponTypes { get; protected set; }
  public override List<ArmorType> AllowedArmorTypes { get; protected set; }
  public override Dictionary<AttributeType, PrimaryAttribute> BasePrimaryAttributes { get; protected set; }
  public override Dictionary<AttributeType, PrimaryAttribute> TotalPrimaryAttributes { get; protected set; }
  public override Dictionary<AttributeType, int> AttributeGain { get; protected set; }
  public override AttributeType MyPrimaryAttribute { get; protected set; }

  public Ranger(string Name) : base(Name)
  {
    int InitialInteligence = 1;
    int InitialDexterity = 7;
    int InitialStrength = 1;

    MyPrimaryAttribute = AttributeType.Dexterity;

    TotalPrimaryAttributes = new()
    {
      {AttributeType.Intelligence, new Intelligence(InitialInteligence) },
      {AttributeType.Dexterity, new Dexterity(InitialDexterity) },
      {AttributeType.Strength, new Strength(InitialStrength) }
    };

    AttributeGain = new()
    {
      { AttributeType.Intelligence, 1 },
      { AttributeType.Dexterity, 5 },
      { AttributeType.Strength, 1 }
    };

    BasePrimaryAttributes = new()
    {
      {AttributeType.Intelligence, new Intelligence(InitialInteligence) },
      {AttributeType.Dexterity, new Dexterity(InitialDexterity) },
      {AttributeType.Strength, new Strength(InitialStrength) }
    };

    Equipment = Character.GetDummyInventoryOfItems();
    AllowedWeaponTypes = new() { WeaponType.Bow };
    AllowedArmorTypes = new() { ArmorType.Leather, ArmorType.Mail };
  }


}