﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1.Characters
{
  public class Mage : Character
  {
    public override Dictionary<ItemSlot, Item> Equipment { get; protected set; }
    public override List<WeaponType> AllowedWeaponTypes { get; protected set; }
    public override List<ArmorType> AllowedArmorTypes { get; protected set; }
    public override Dictionary<AttributeType, PrimaryAttribute> BasePrimaryAttributes { get; protected set; }
    public override Dictionary<AttributeType, PrimaryAttribute> TotalPrimaryAttributes { get; protected set; }
    public override Dictionary<AttributeType, int> AttributeGain { get; protected set; }
    public override AttributeType MyPrimaryAttribute { get; protected set; }

    public Mage(string Name) : base(Name)
    {
      int InitialInteligence = 8;
      int InitialDexterity = 1;
      int InitialStrength = 1;

      MyPrimaryAttribute = AttributeType.Intelligence;

      TotalPrimaryAttributes = new()
    {
      {AttributeType.Intelligence, new Intelligence(InitialInteligence) },
      {AttributeType.Dexterity, new Dexterity(InitialDexterity) },
      {AttributeType.Strength, new Strength(InitialStrength) }
    };

      AttributeGain = new()
    {
      { AttributeType.Intelligence, 5 },
      { AttributeType.Dexterity, 1 },
      { AttributeType.Strength, 1 }
    };

      BasePrimaryAttributes = new()
    {
      {AttributeType.Intelligence, new Intelligence(InitialInteligence) },
      {AttributeType.Dexterity, new Dexterity(InitialDexterity) },
      {AttributeType.Strength, new Strength(InitialStrength) }
    };

      Equipment = GetDummyInventoryOfItems();
      AllowedWeaponTypes = new() { WeaponType.Staff, WeaponType.Wand };
      AllowedArmorTypes = new() { ArmorType.Cloth };
    }

  }
}