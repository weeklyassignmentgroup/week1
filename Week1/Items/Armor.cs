﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Week1.Items;

/// <summary>
/// Armor is an item characters can equip to increase their primarystats.
/// </summary>
public class Armor : Item
{
  public ItemSlot UsedItemslot { get; }
  public int RequiredLevel { get; }
  public string Name { get; }
  public ArmorType Type { get; }
  public Dictionary<AttributeType, PrimaryAttribute> AttributeBonus { get; }

  private static List<ItemSlot> UsableSlots { get; }  = new()
    {
      ItemSlot.Head,
      ItemSlot.Legs,
      ItemSlot.Body
    };

  /// <summary>
  ///  Creates an armorpiece
  /// </summary>
  /// <param name="type"></param> Each character has certain armor-types it can equip
  /// <param name="name"></param> 
  /// <param name="requiredLevel"></param> Can be used to exclude low level characters from wearing it
  /// <param name="usableSlot"></param> Armor can be worn in many slots, each piece is dedicated to one of them
  /// <param name="attributeBonus"></param> This is the bonus to primary attributes it gives when worn, 
  ///                                       values between 1 And MaxCap are enforced inside the attribute class
  /// <exception cref="ArgumentException"></exception> Incase you try to make an armor item with an illegal itemslot like weapon
  public Armor(ArmorType type, string name, int requiredLevel, ItemSlot usableSlot, Dictionary<AttributeType, PrimaryAttribute> attributeBonus)
  {
    this.Type = type;
    this.Name = name;
    this.RequiredLevel = requiredLevel;
    this.AttributeBonus = attributeBonus;

    if (!UsableSlots.Contains(usableSlot)) { throw new InvalidArmorException("Armor cannot be created for that itemslot"); }  
    this.UsedItemslot = usableSlot; //Do a check that it is in fact one of the usuable slots
    
  }

  /// <summary>
  /// Summary of the main aspects of this Armorpiece
  /// </summary>
  /// <returns>An armordescription spanning multiple lines</returns>
  public override string ToString()
  {
    StringBuilder armorDescription = new StringBuilder()
      .AppendLine($"Name: {Name}")
      .AppendLine($"Type: {Type}")
      .AppendLine($"RequiredLevel: {RequiredLevel}");

    foreach (AttributeType atrType in AttributeBonus.Keys)
    {
      armorDescription.AppendLine($"{atrType}: {AttributeBonus[atrType]}");
    }


    return armorDescription.ToString();
  }


}

