﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public enum ItemSlot
{
  Head,
  Body,
  Legs,
  Weapon
}
