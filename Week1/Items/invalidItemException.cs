﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1.Items
{
  public class InvalidItemException : Exception
  {

    ///Constuctor med string
    public InvalidItemException(string message) : base(message)
    {

    }

  }
}
