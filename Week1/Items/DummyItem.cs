﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// A dummy item to represent an empty itemslot
/// This is used to avoid a lot of null-checks 
/// This item will never affect the character in any way.
/// </summary>
public class DummyItem : Item
{

  public int RequiredLevel => 0;

  public string Name => "";

  public ItemSlot UsedItemslot { get; }
}
