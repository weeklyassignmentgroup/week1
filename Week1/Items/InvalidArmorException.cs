﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1.Items
{
  /// <summary>
  /// Custom Exception to be thrown when someone tries to equip an invalid or illegal armor
  /// </summary>
  public class InvalidArmorException : Exception
  {
    public InvalidArmorException(string message) : base(message)
    {

    }
  }
}
