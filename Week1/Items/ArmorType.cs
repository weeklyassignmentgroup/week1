﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The different kinds of Armor
/// </summary>
public enum ArmorType
{
  Cloth,
  Leather,
  Mail,
  Plate
}
