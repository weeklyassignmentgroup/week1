﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1.Items
{
  /// <summary>
  /// Custom Exception to be thrown when someone tries to equip an invalid or illegal weapon
  /// </summary>
  public class InvalidWeaponException : Exception
  {
    public InvalidWeaponException(string message) : base(message)
    {

    }

  }
}
