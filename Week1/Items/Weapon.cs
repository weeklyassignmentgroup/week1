﻿using System;
using System.Text;

public class Weapon : Item
{
  //from item interface
  private static List<ItemSlot> UsableSlots { get; set; } = new() { ItemSlot.Weapon };
  public int RequiredLevel { get; private set; }
  public string Name { get; private set; }
  public ItemSlot UsedItemslot { get; }

  //Weapon Specific
  public WeaponType Type { get; private set; }
  public double AttacksPerSecound { get; private set; }
  public double Damage { get; private set; }


  /// <summary>
  /// Creates a Weapon which is an item that can be wielded by characters in the weapon itemslot
  /// It does not permit negative values for damage and attackspeed as no weapon should ever do negative dps
  /// Which is what these two are used to calculate. 
  /// </summary>
  /// <param name="type"></param> each character can only wield certain itemtypes
  /// <param name="damage"></param> how much a hit damages
  /// <param name="attackSpeed"></param> long between each hit
  /// <param name="name"></param> 
  /// <param name="requiredLevel"></param> this can be used to prevent low level heroes from equipping the item
  /// <exception cref="ArgumentException">Thrown if you sent negative values for damage or attackspeed</exception>
  public Weapon(WeaponType type, double damage, double attackSpeed, string name, int requiredLevel)
  {
    Type = type;
    if (damage <= 0 || attackSpeed <= 0)
    {
      throw new ArgumentException("You need more than 0 damage and attackspeed on weapons");
    }
    Damage = damage;
    AttacksPerSecound = attackSpeed;
    Name = name;
    RequiredLevel = requiredLevel;
    //This attribute can be put in constuctor if weapons are allowed to be equipped in other slots at a later date
    UsedItemslot = ItemSlot.Weapon;

  }

  /// <summary>
  /// Returns the DPS, which is used to calculate damage done by characters
  /// </summary>
  /// <returns>Damage per secound done by this weapon</returns>
  public double CalculateDamagePerSecound()
  {
    return Damage*AttacksPerSecound;
  }

  /// <summary>
  /// This is used to print out the item description of this weapon
  /// Usefull for characters when deciding if they want to use it and if they can use it.
  /// </summary>
  /// <returns>Description of the most important aspects of the weapon spanning multiple lines</returns>
  public override string ToString()
  {
    StringBuilder weaponDescription = new StringBuilder()
        .AppendLine($"Name: {Name}")
        .AppendLine($"Type: {Type}")
        .AppendLine($"Damage: {Damage}")
        .AppendLine($"AttackSpeed: {AttacksPerSecound}")
        .AppendLine($"Required level: {RequiredLevel}");

    return weaponDescription.ToString();
  }

}
