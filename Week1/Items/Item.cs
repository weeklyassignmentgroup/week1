﻿using System;

/// <summary>
/// An interface representing the common attributes of items
/// An Item is something which can be picked up and used by a character
/// It uses one itemslot, and requires a certain level to use.
/// </summary>
public interface Item
{
  /// <summary>
  /// The required level to equip this item
  /// </summary>
  public int RequiredLevel { get; }
  /// <summary>
  /// The name of the item
  /// </summary>
  public string Name { get; }
   /// <summary>
   /// The slot the item has to be worn in
   /// </summary>
  public ItemSlot UsedItemslot { get; }

}
