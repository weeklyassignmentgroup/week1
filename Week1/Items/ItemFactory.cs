﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1.Items
{
  /// <summary>
  /// Creates items both for the RPG and for testing
  /// Made to have a central location that can adapt when changes happen
  /// </summary>
  public class ItemFactory
  {
    /// <summary>
    /// Default values for each, for central creation of items and is especially good in testing scenarios
    /// I do not wish the constructor of the weapon or armor itself to have defaulted values,
    /// because it could give off the wrong impression that you do not need to change the values
    /// This applies to both MakeArmor and MakeWeapon
    /// </summary>
    public static Armor MakeArmor(ArmorType type = ArmorType.Cloth, string name = "test",
    int requiredLevel = 1, ItemSlot itemSlot = ItemSlot.Head, Dictionary<AttributeType, PrimaryAttribute>?bonusAttributes = null)
    {
      if (bonusAttributes == null) { 
         bonusAttributes = new() {
          { AttributeType.Intelligence, new Intelligence(1) },
          { AttributeType.Strength, new Strength(1) },
          { AttributeType.Dexterity, new Dexterity(1) },
        };
      }
      return new Armor(type, name, requiredLevel, itemSlot, bonusAttributes);
    }
 

    public static Weapon MakeWeapon(WeaponType type = WeaponType.Axe, double damage = 3,
     double attackspeed = 3, string name = "test", int requiredLevel = 1)
    {
      return new Weapon(type, damage, attackspeed, name, requiredLevel);
    }

  }
}
