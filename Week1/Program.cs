﻿// See https://aka.ms/new-console-template for more information
using System.Collections.Generic;
using Week1.Characters;
using Week1.Monster;


/*
 * Define all the base classes
 * 
 */


Dictionary<MonsterType, Monster> monsters = new Dictionary<MonsterType, Monster>()
{
   { MonsterType.Dragon, new Monster(monsterType: MonsterType.Dragon,
  level: 2, health: 20, damage: 4) },
  { MonsterType.Baselisk, new Monster(monsterType: MonsterType.Baselisk,
  level: 1, health: 1, damage: 3) },
    { MonsterType.DeadKitten, new Monster(monsterType: MonsterType.DeadKitten,
  level: 1, health: 0, damage: 1) }
};

Dictionary<RoomType, Room> rooms = new Dictionary<RoomType, Room>()
{
  { RoomType.Dungeon, new Dungeon(monsters[MonsterType.Baselisk]) },
  { RoomType.Keep, new Keep(monsters[MonsterType.Dragon]) },
  { RoomType.Hallway, new Hallway(monsters[MonsterType.DeadKitten]) },

};

//Only one character representing each type like in Diablo
Dictionary<CharacterType, Character?> characters = new() {
  { CharacterType.Mage, null },
};




Character character = characterSetup();

GameLoop(character);

Character characterSetup()
{
  Console.WriteLine("Welcome traveler, I am Sloan");
  Console.WriteLine(" ** Give your character a name **");
  string GivenName = Console.ReadLine();
  CharacterType characterType = CharacterType.Warrior;
  while (true)
  {
    Console.WriteLine($"I wonder what your specialty is {GivenName}");
    Console.WriteLine("Are you any good with magics? ** y/n **");
    ConsoleKeyInfo answer = Console.ReadKey();
    if (answer.Key == ConsoleKey.Y) { characterType = CharacterType.Mage; break;}
    Console.WriteLine($"No magic eyh, I bet you love to go bowhunting? ** y/n **");
    answer = Console.ReadKey();
    if (answer.Key == ConsoleKey.Y) {  characterType = CharacterType.Ranger; break;
    }
    Console.WriteLine($"I gues you like to sneak about then? ** y/n **");
    answer = Console.ReadKey();
    if (answer.Key == ConsoleKey.Y) {  characterType = CharacterType.Rogue; break; }
    else { 
      Console.WriteLine($"I see now you are a Warrior");
      
      characterType = CharacterType.Warrior;
      break;
    }
  }
  Character character = CharacterFactory.GetCharacter(CharacterType.Mage, GivenName);
  Console.WriteLine($"A {characterType} like you could maby help me get rid of some beasts in my home");
  Console.WriteLine($"I will show you to my house, come with me {character.Name}\n");
  return character;
}

void GameLoop(Character character)
{
  bool won = false;
  bool alive = true;
  Room currentRoom = rooms[RoomType.Hallway];


  while (!won && alive)
  {
    currentRoom.EnterRoom();

    if (!currentRoom.IsCleared) { 
      bool wannaAttack = chooseAttack();
      if (wannaAttack)
      {
        currentRoom.AttackBoss(character);
        List<Item> loot = currentRoom.Loot();
        loot.ForEach(item => {
          if(character.CanEquip(item))
            character.Equip(item);
        });
        Console.WriteLine("Your characters stats summary after fight \n" + character.ToString());

        alive = character.IsAlive();
      }
      
    }

    won = CheckWinCondition();

    if(!won) currentRoom = chooseRoom(currentRoom);
  

  }
  if (alive = false)
  {
    Console.WriteLine("You had a good run");
  }
  else
  {
    Console.WriteLine("Congratulations you won");
  }
}


Room chooseRoom(Room currentRoom)
{
  Console.WriteLine("Where do you wanna go? Type it's name exactly");
  bool chosenRoom = false;
  while (chosenRoom == false)
  {

    foreach (Room room in rooms.Values)
    {
      Console.WriteLine(room.CurrentRoomType);
    }
    string roomChoice = Console.ReadLine();

    RoomType choosenRoom;
    Enum.TryParse<RoomType>(roomChoice, true, out choosenRoom);

    if (rooms.ContainsKey(choosenRoom))
    {
      return rooms[choosenRoom];
    }
    else
    {
      Console.WriteLine("You didn't type the key Exactly I am sorry try again");
    }
  }
  return currentRoom;
}

bool chooseAttack()
{
  Console.WriteLine("Do you wish to attack the Boss? yes or no");
  while (true)
  {
    string answer = Console.ReadLine();
    if (answer.Equals("yes"))
    {
      return true;
    }
    else if (answer.Equals("no"))
    {
      return false;
    }
    else
    {
      Console.WriteLine("You have to answer exactly yes or exactly no");
    }
  }

}


bool CheckWinCondition()
{
  bool won = false;
  int ClearedRooms = 0;
  foreach (Room room in rooms.Values)
  {
    if (room.IsCleared)
    {
      ClearedRooms++;
    }
  }
  if (ClearedRooms == rooms.Count)
  {
    won = true;
  }
  return won;
}









