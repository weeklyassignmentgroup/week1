﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1.Attributes
{
  public class AttributeFactory
  {
    /// <summary>
    /// Make a primaryattribute by sending in a enum
    /// </summary>
    /// <param name="attributeType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static PrimaryAttribute MakePrimaryAttribute(AttributeType attributeType, int amount=1)
    {

      switch (attributeType)
      {
        case (AttributeType.Strength):
        {
          return new Strength(amount);
        }
        case (AttributeType.Intelligence):
        {
          return new Intelligence(amount);
        }
        case (AttributeType.Dexterity):
        {
          return new Dexterity(amount);
        }
        default:
        {
          throw new ArgumentException("unsupported Attribute");
        }
      }
    }
  }
}
