﻿using System;

/// <summary>
/// This interface represents the primary attribues
/// It is not a abstract class because structs are not able to inherit, 
/// and I think the attributes themselves should be value types that implement this
/// I think this setup is good for beeing extensible in adding new Attributes although abit verbose
/// This more independent solution enables characters to completely lack some primary attributes
/// </summary>
public interface PrimaryAttribute
{
  
  const int MaxAttribute = 255;
  //1-255
	public int Amount { get; protected set; }
  /// <summary>
  /// Adds attributevalue, should be capped at constant cap MaxAttribute
  /// Anything gained above that puts the amount at cap.
  /// </summary>
  /// <param name="gain"></param>
  public void AddAttribute(int gain);

  /// <summary>
  /// A shortcut to adding two of the same attribute
  /// Only works if both primary attributes are the same type
  /// </summary>
  /// <param name="gain">A primary attribute of the same type</param>
  public void AddAttribute(PrimaryAttribute gain);



  /// <summary>
  /// Subtracts an attribute by detraction amount, should never become 0 or negative value
  /// </summary>
  /// <param name="detraction"></param>
  /// <exception cref="AttributeException"></exception>
  public void SubtractAttribute(int detraction);

  /// <summary>
  /// A shortcut to subtract two of the same attribute
  /// Only works if both primary attributes are the same type
  /// </summary>
  /// <param name="detraction">A primary attribute of the same type</param>
  public void SubtractAttribute(PrimaryAttribute detraction);


}
