﻿using System;
using Week1.Attributes;

public struct Intelligence : PrimaryAttribute
{
  public int Amount { get; set; }

	public AttributeType AttributeType { get; } = AttributeType.Intelligence;

	public Intelligence(int baseAttributeAmount)
	{
    Amount = baseAttributeAmount;
  }


	public  void AddAttribute(int gain)
	{
    if (PrimaryAttribute.MaxAttribute < (Amount + gain))
    {
      Amount = PrimaryAttribute.MaxAttribute;
    }
    else
    {
      Amount += gain;
    }
  }

  public void AddAttribute(PrimaryAttribute gain)
  {
    if (gain.GetType() == typeof(Intelligence))
      AddAttribute(gain.Amount);
    else throw new ArgumentException("You can only add alike primary attributes this way");
  }

  public void SubtractAttribute(int detraction)
	{
    if (Amount <= detraction) throw new AttributeException();
    Amount -= detraction;
	}

  public void SubtractAttribute(PrimaryAttribute detraction)
  {
    if (detraction.GetType() == typeof(Intelligence))
      SubtractAttribute(detraction.Amount);
    else throw new ArgumentException("You can only subtract alike primary attributes this way");
  }

}
