﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Items;
using Week1.Monster;

public class Keep : Room
{
  /// <summary>
  /// The roomtype of this room
  /// </summary>
  public override RoomType CurrentRoomType { get; } = RoomType.Keep;
  
  /// <summary>
  /// The loot of this room, will be retireved when the room is cleared of monsters
  /// </summary>
  public List<Item> LootItems = new() {
    ItemFactory.MakeWeapon(WeaponType.Axe, 30, 1, "AxeBringer", 1),
    ItemFactory.MakeArmor(type: ArmorType.Cloth, name: "CloakOfSmoothness", requiredLevel: 2,
    bonusAttributes: new (){{ AttributeType.Intelligence, new Intelligence(3)}, 
      {AttributeType.Strength, new Strength(1)}, {AttributeType.Dexterity, new Dexterity(2)}})
  };

  public Keep(Monster monster) : base(monster)
  {
    
  }

  /// <summary>
  /// The introduction to this room when someone enters it 
  /// </summary>
  public override void EnterRoom()
  {
    Console.WriteLine($"You have entered the keep, there is a hidious {Boss.Type} in front of you it has level {Boss.Level}");
  }

  public override List<Item> Loot()
  {
    return LootItems;
  }
}

