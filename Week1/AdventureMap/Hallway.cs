﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Monster;

public class Hallway : Room
{
  public override RoomType CurrentRoomType { get; } = RoomType.Hallway;

  public Hallway(Monster monster) : base(monster)
  {
    IsCleared = true;
  }

  public override void EnterRoom()
  {
    Console.WriteLine("You enter the hallway and wonder whats behind all these doors");
  }

  public override List<Item> Loot()
  {
    return new();
  }
}

