﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Monster;

public abstract class Room
{
  /// <summary>
  /// A monster that lives in the room and guards it's treasures
  /// </summary>
  public Monster Boss { get; protected set; }
  ///
  public abstract RoomType CurrentRoomType { get; }

  /// <summary>
  /// Creates a room
  /// </summary>
  /// <param name="boss">A monster inside the room</param>
  public Room(Monster boss){
    Boss = boss;
  }

  /// <summary>
  /// This tracks if the room is clear of all bosses
  /// </summary>
  public bool IsCleared { get; protected set; } = false;

  /// <summary>
  /// This method is used to implement what happens when someone enters the room
  /// </summary>
  public abstract void EnterRoom();

  /// <summary>
  /// A character tries to attack the boss
  /// If the boss survives it will retaliate
  /// If it dies character levels up.
  /// </summary>
  /// <param name="character"></param>
  public void AttackBoss(Character character)
  {
    double damage = character.CalculateDamage();
    IsCleared = !(Boss.TakeDamageReturnIsAlive(damage));

    if (IsCleared)
    {
      Console.WriteLine($"You have killed the {Boss.Type}!");
      character.LevelUp();
    }
    else
    {
      Console.WriteLine($"You hit for {damage} and the boss retaliates for {Boss.Damage}");
      character.Health -= Boss.AttackDamageIfAlive();
    }
  }

  /// <summary>
  /// The loot contained in the room
  /// These can be looted when the room is cleared
  /// </summary>
  /// <returns></returns>
  public abstract List<Item> Loot();

}

