﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Items;
using Week1.Monster;


public class Dungeon : Room
{
  public override RoomType CurrentRoomType { get; } = RoomType.Dungeon;

  public List<Item> LootItems = new() {
    ItemFactory.MakeWeapon(WeaponType.Staff, 100, 4, "StaffMan", 1),
    ItemFactory.MakeArmor(type: ArmorType.Plate, name: "HardBreastPlate", requiredLevel: 2,
      bonusAttributes: new (){{ AttributeType.Intelligence, new Intelligence(1)}, {AttributeType.Strength, new Strength(6)}, {AttributeType.Dexterity, new Dexterity(2)}}),
    ItemFactory.MakeArmor(type: ArmorType.Leather, name: "LeatheryHeadress", requiredLevel: 1,
      bonusAttributes: new (){{ AttributeType.Intelligence, new Intelligence(0)}, {AttributeType.Strength, new Strength(1) }, {AttributeType.Dexterity, new Dexterity(6)}}),
    ItemFactory.MakeArmor(type: ArmorType.Mail, name: "RivitedChest", requiredLevel: 1,
      bonusAttributes: new (){{ AttributeType.Intelligence, new Intelligence(0)}, {AttributeType.Strength, new Strength(4)}, {AttributeType.Dexterity, new Dexterity(4)}})
  };

  public Dungeon(Monster monster) : base(monster)
  {
    

  }

  public override void EnterRoom()
  {
    Console.WriteLine("You have entered the dungeon, there is a hidious Basilisk in front of you");
  }

  public override List<Item> Loot()
  {
    return LootItems;
  }
}

