﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Attributes;
using Week1.Characters;
using Week1.Items;

namespace TestRPGAssignment.Attributes
{
  public class AttributeTests
  {
    /// <summary>
    /// Initializes all attributes with amount 1
    /// </summary>
    /// <returns></returns>
    public static IEnumerable<object[]> AllAttributesAmount1()
    {
      foreach (AttributeType attributeType in Enum.GetValues(typeof(AttributeType)))
      {
        yield return new[] { AttributeFactory.MakePrimaryAttribute(attributeType) }; 
      }
    }

    [Theory]
    [MemberData(nameof(AllAttributesAmount1))]
    public void SubtractAtttribute_SubtractAttributesSoYouGoIntoNegatives_ThrowAttributeException(PrimaryAttribute attribute)
    {
      //Act && Assert
      Assert.Throws<AttributeException>(() => attribute.SubtractAttribute(attribute.Amount+2));

    }

    [Theory]
    [MemberData(nameof(AllAttributesAmount1))]
    public void SubtractAtttribute_SubtractAttributesToZero_ThrowAttributeException(PrimaryAttribute attribute)
    {
      //Act && Assert
      Assert.Throws<AttributeException>(() => attribute.SubtractAttribute(attribute.Amount));

    }

    [Theory]
    [MemberData(nameof(AllAttributesAmount1))]
    public void AddAtttribute_AddAttributeBeyondMaxCap_AttributeSetAsMaxCap(PrimaryAttribute attribute)
    {
      //Arrange
      int expected = PrimaryAttribute.MaxAttribute;
      int addedAmount = PrimaryAttribute.MaxAttribute+ 1;
      //Act
      attribute.AddAttribute(addedAmount);
      int actual = attribute.Amount;
      //Assert
      Assert.Equal(expected, actual);
    }

    [Theory]
    [MemberData(nameof(AllAttributesAmount1))]
    public void AddAtttribute_Attribute1Add1_Two(PrimaryAttribute attribute)
    {
      //Arrange
      int expected = 2;
      int addedAmount = 1;
      //Act
      attribute.AddAttribute(addedAmount);
      int actual = attribute.Amount;
      //Assert
      Assert.Equal(expected, actual);
    }

    [Theory]
    [MemberData(nameof(AllAttributesAmount1))]
    public void AddAtttribute_Attribute1Add1ByUsingSameTypeOfPrimaryAttribute_Two(PrimaryAttribute attribute)
    {
      //Arrange
      int expected = 2;
      //Act
      attribute.AddAttribute(attribute);
      int actual = attribute.Amount;
      //Assert
      Assert.Equal(expected, actual);
    }

    [Fact]
    public void AddAtttribute_Attribute1Add1ByUsingDifferentTypeOfPrimaryAttribute_ArgumentException()
    {
      //Arrange
      PrimaryAttribute inteligence = AttributeFactory.MakePrimaryAttribute(AttributeType.Intelligence, 1);
      PrimaryAttribute strength = AttributeFactory.MakePrimaryAttribute(AttributeType.Strength, 1);

      //Act && Assert
      Assert.Throws<ArgumentException>(() => inteligence.AddAttribute(strength));
    }

    [Fact]
    public void SubtractAtttribute_Attribute5Subtract1ByUsingSameTypeOfPrimaryAttribute_Four()
    {
      //Arrange
      int initialAmount = 5;
      int subtractedAmount = 1;
      int expected = 4;
      PrimaryAttribute primary = AttributeFactory.MakePrimaryAttribute(AttributeType.Intelligence, initialAmount);
      PrimaryAttribute subtractionPrimary = AttributeFactory.MakePrimaryAttribute(AttributeType.Intelligence, subtractedAmount);

      //Act
      primary.SubtractAttribute(subtractionPrimary);
      int actual = primary.Amount;
      //Assert
      Assert.Equal(expected, actual);
    }

    [Fact]
    public void SubtractAtttribute_Attribute5Subtract1ByUsingDifferentTypeOfPrimaryAttribute_ArgumentException()
    {
      //Arrange
      int initialAmount = 5;
      int subtractedAmount = 1;
      PrimaryAttribute intellgience = AttributeFactory.MakePrimaryAttribute(AttributeType.Intelligence, initialAmount);
      PrimaryAttribute strength = AttributeFactory.MakePrimaryAttribute(AttributeType.Strength, subtractedAmount);

      //Assert
      Assert.Throws<ArgumentException>(() => intellgience.SubtractAttribute(strength));
    }

  }
}
