﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Characters;
using Week1.Items;

namespace TestRPGAssignment.Characters
{
  public class ExperimentalTests
  {
    /// <summary>
    /// These tests make sure characters get error when attempting to equip illegal weapons
    /// They are therefore quite alike.
    /// </summary>
    /// <param name="character"></param> come from collections
    public static IEnumerable<object[]> IllegalAxeCharacters()
    {
      yield return new object[] { CharacterFactory.GetMage() };
      yield return new object[] { CharacterFactory.GetRogue() };
      yield return new object[] { CharacterFactory.GetRanger() };
    }
    public static IEnumerable<object[]> IllegalSwordCharacters()
    {
      yield return new object[] { CharacterFactory.GetMage() };
      yield return new object[] { CharacterFactory.GetRanger() };
    }
    public static IEnumerable<object[]> IllegalStaffCharacters()
    {
      yield return new object[] { CharacterFactory.GetWarrior() };
      yield return new object[] { CharacterFactory.GetRogue() };
      yield return new object[] { CharacterFactory.GetRanger() };
    }
    public static IEnumerable<object[]> IllegalBowCharacters()
    {
      yield return new object[] { CharacterFactory.GetWarrior() };
      yield return new object[] { CharacterFactory.GetRogue() };
      yield return new object[] { CharacterFactory.GetMage() };
    }
    public static IEnumerable<object[]> IllegalDaggerCharacters()
    {
      yield return new object[] { CharacterFactory.GetWarrior() };
      yield return new object[] { CharacterFactory.GetRanger() };
      yield return new object[] { CharacterFactory.GetMage() };
    }
    public static IEnumerable<object[]> IllegalHammerCharacters()
    {
      yield return new object[] { CharacterFactory.GetRogue() };
      yield return new object[] { CharacterFactory.GetRanger() };
      yield return new object[] { CharacterFactory.GetMage() };
    }
    public static IEnumerable<object[]> IllegalWandCharacters()
    {
      yield return new object[] { CharacterFactory.GetRogue() };
      yield return new object[] { CharacterFactory.GetRanger() };
      yield return new object[] { CharacterFactory.GetWarrior() };
    }
    //MemberData works with sending an array into One test
    [Theory]
    [MemberData(nameof(IllegalAxeCharacters))]
    public void Equip_CharacterEquipsIllegalAxe_ThrowsIllegalWeaponException(Character character)
    {
      //Arrenge (including expected)
      Weapon TestAxe = new Weapon(WeaponType.Axe, 5, 5, "Common Axe", 1);

      //Assert && Act
      Assert.Throws<InvalidWeaponException>(() => character.Equip(TestAxe));
    }

    //MemberData works with sending an array into One test
    [Theory]
    [MemberData(nameof(IllegalSwordCharacters))]
    public void Equip_CharacterEquipsIllegalSword_ThrowsIllegalWeaponException(Character character)
    {
      //Arrenge (including expected)
      Weapon TestSword = new Weapon(WeaponType.Sword, 5, 5, "Common Sword", 1);

      //Assert && Act
      Assert.Throws<InvalidWeaponException>(() => character.Equip(TestSword));
    }

    //MemberData works with sending an array into One test
    [Theory]
    [MemberData(nameof(IllegalWandCharacters))]
    public void Equip_CharacterEquipsIllegalWand_ThrowsIllegalWeaponException(Character character)
    {
      //Arrenge (including expected)
      Weapon TestWand = new Weapon(WeaponType.Wand, 5, 5, "Common Wand", 1);

      //Assert && Act
      Assert.Throws<InvalidWeaponException>(() => character.Equip(TestWand));
    }

    //MemberData works with sending an array into One test
    [Theory]
    [MemberData(nameof(IllegalHammerCharacters))]
    public void Equip_CharacterEquipsIllegalHammer_ThrowsIllegalWeaponException(Character character)
    {
      //Arrenge (including expected)
      Weapon TestHammer = new Weapon(WeaponType.Hammer, 5, 5, "Common Hammer", 1);

      //Assert && Act
      Assert.Throws<InvalidWeaponException>(() => character.Equip(TestHammer));
    }

    //MemberData works with sending an array into One test
    [Theory]
    [MemberData(nameof(IllegalStaffCharacters))]
    public void Equip_CharacterEquipsIllegalStaff_ThrowsIllegalWeaponException(Character character)
    {
      //Arrenge (including expected)
      Weapon TestStaff = new Weapon(WeaponType.Staff, 5, 5, "Common Staff", 1);

      //Assert && Act
      Assert.Throws<InvalidWeaponException>(() => character.Equip(TestStaff));
    }

    //MemberData works with sending an array into One test
    [Theory]
    [MemberData(nameof(IllegalDaggerCharacters))]
    public void Equip_CharacterEquipsIllegalDagger_ThrowsIllegalWeaponException(Character character)
    {
      //Arrenge (including expected)
      Weapon TestDagger = new Weapon(WeaponType.Dagger, 5, 5, "Common Dagger", 1);

      //Assert && Act
      Assert.Throws<InvalidWeaponException>(() => character.Equip(TestDagger));
    }

    //MemberData works with sending an array into One test
    [Theory]
    [MemberData(nameof(IllegalBowCharacters))]
    public void Equip_CharacterEquipsIllegalBow_ThrowsIllegalWeaponException(Character character)
    {
      //Arrenge (including expected)
      Weapon TestBow = new Weapon(WeaponType.Bow, 5, 5, "Common Bow", 1);

      //Assert && Act
      Assert.Throws<InvalidWeaponException>(() => character.Equip(TestBow));
    }

    public static IEnumerable<object[]> IllegalCharacterForClothArmor()
    {
      yield return new object[] { CharacterFactory.GetWarrior() };
      yield return new object[] { CharacterFactory.GetRogue() };
      yield return new object[] { CharacterFactory.GetRanger() };
    }

    public static IEnumerable<object[]> IllegalCharacterForLeatherArmor()
    {
      yield return new object[] { CharacterFactory.GetWarrior() };
      yield return new object[] { CharacterFactory.GetMage() };
    }

    public static IEnumerable<object[]> IllegalCharacterForMailArmor()
    {
      yield return new object[] { CharacterFactory.GetMage() };
    }

    public static IEnumerable<object[]> IllegalCharacterForPlateArmor()
    {
      yield return new object[] { CharacterFactory.GetMage() };
      yield return new object[] { CharacterFactory.GetRogue() };
      yield return new object[] { CharacterFactory.GetRanger() };
    }

    /// <summary>
    /// These tests make sure the different charachters get illegalArmorException
    /// When trying to equip armor they cannot
    /// They are therefore quite alike.
    /// </summary>
    /// <param name="character"></param> come from collections
    //MemberData works with sending an array into One test
    [Theory]
    [MemberData(nameof(IllegalCharacterForClothArmor))]
    public void Equip_CharacterEquipsIllegalClothArmor_ThrowsIllegalArmorException(Character character)
    {
      //Arrenge (including expected)
      Armor ClothArmor = ItemFactory.MakeArmor(type: ArmorType.Cloth);

      //Assert && Act
      Assert.Throws<InvalidArmorException>(() => character.Equip(ClothArmor));
    }
    [Theory]
    [MemberData(nameof(IllegalCharacterForLeatherArmor))]
    public void Equip_CharacterEquipsIllegalLeatherArmor_ThrowsIllegalArmorException(Character character)
    {
      //Arrenge (including expected)
      Armor LeatherArmor = ItemFactory.MakeArmor(type: ArmorType.Leather);

      //Assert && Act
      Assert.Throws<InvalidArmorException>(() => character.Equip(LeatherArmor));
    }

    [Theory]
    [MemberData(nameof(IllegalCharacterForMailArmor))]
    public void Equip_CharacterEquipsIllegalMailArmor_ThrowsIllegalArmorException(Character character)
    {
      //Arrenge (including expected)
      Armor MailArmor = ItemFactory.MakeArmor(type: ArmorType.Mail);

      //Assert && Act
      Assert.Throws<InvalidArmorException>(() => character.Equip(MailArmor));
    }

    [Theory]
    [MemberData(nameof(IllegalCharacterForPlateArmor))]
    public void Equip_CharacterEquipsIllegalPlateArmor_ThrowsIllegalArmorException(Character character)
    {
      //Arrenge (including expected)
      Armor PlateArmor = ItemFactory.MakeArmor(type: ArmorType.Plate);

      //Assert && Act
      Assert.Throws<InvalidArmorException>(() => character.Equip(PlateArmor));
    }
  }
}
