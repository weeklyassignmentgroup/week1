﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestPlatform.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Characters;
using Week1.Items;
using Xunit.Abstractions;

namespace TestRPGAssignment.Characters
{



  public class EquipAndItemTest 
  {

    //Tests to make sure you gain stats when equipping items
    [Fact]
    public void Equip_CharacterTriesToEquipToHighLevelWeapon_ThrowsInvalidItemException()
    {
      //Arrange
      Warrior testWarrior = CharacterFactory.GetWarrior(); //All are lvl 1
      Weapon weapon = ItemFactory.MakeWeapon(type: WeaponType.Axe,requiredLevel: 2);

      //Assert && Arrange
      Assert.Throws<InvalidItemException>(() => testWarrior.Equip(weapon));

    }

    [Fact]
    public void Equip_CharacterTriesToEquipToHighLevelArmor_ThrowsInvalidArmorException()
    {
      //Arrange
      Warrior testWarrior = CharacterFactory.GetWarrior(); //All are lvl 1
      Armor armor = ItemFactory.MakeArmor(type: ArmorType.Plate, requiredLevel: 2);

      //Assert && Arrange
      Assert.Throws<InvalidItemException>(() => testWarrior.Equip(armor));

    }

    [Fact]
    public void Equip_WarriorEquipsInvalidBow_ThrowsInvalidWeaponException()
    {
      //Arrenge (including expected)
      Weapon TestBow = new Weapon(WeaponType.Bow, 5, 5, "Common Bow", 1);
      Warrior TestWarrior = CharacterFactory.GetWarrior();

      //Assert && Act
      Assert.Throws<InvalidWeaponException>(() => TestWarrior.Equip(TestBow));
    }

    [Fact]
    public void Equip_WarriorEquipsInvalidClothArmor_ThrowsInvalidArmorException()
    {
      //Arrenge (including expected)
      Armor ClothArmor = ItemFactory.MakeArmor(type: ArmorType.Cloth);
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);

      //Assert && Act
      Assert.Throws<InvalidArmorException>(() => TestWarrior.Equip(ClothArmor));
    }

    [Fact]
    public void Equip_WarriorSuccuessfullyEquipsAxeGetsMessage_NewWeaponEquipped()
    {
      //Arrange
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);
      Weapon TestAxe = ItemFactory.MakeWeapon(type: WeaponType.Axe);
      string expected = "New weapon equipped!";
      //Act
      string actual = TestWarrior.Equip(TestAxe);
      //Assert
      Assert.Equal(expected, actual);


    }
    [Fact]
    public void Equip_WarriorSuccuessfullyEquipsArmorGetsMessage_NewArmorEquipped()
    {
      //Arrange
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);
      Armor TestArmorPlate = ItemFactory.MakeArmor(type: ArmorType.Plate);
      string expected = "New Armor equipped!";
      //Act
      string actual = TestWarrior.Equip(TestArmorPlate);
      //Assert
      Assert.Equal(expected, actual);
    }

    [Fact]
    public void CalculateDamage_CalcDamageForUnarmedWarriorLevelOne_1DPSWith5PercentBonus()
    {
      //Arrange
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);
      double expected = 1 * (1.0 + (TestWarrior.BasePrimaryAttributes[AttributeType.Strength].Amount / 100.0));
      double margin = 0.00005;
      //act
      double actual = TestWarrior.CalculateDamage();

      //assert
      Assert.True(Math.Abs(actual - expected) < margin);

    }

    [Fact]
    public void CalculateDamage_Level1WarriorWithValid5DPSAxeEquipped_5DPSWith5PercentBonus()
    {
      //Arrange
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);
      Weapon weapon = ItemFactory.MakeWeapon(attackspeed: 1, damage: 5,type: WeaponType.Axe);
      
      TestWarrior.Equip(weapon);
      double dps = weapon.AttacksPerSecound * weapon.Damage;
      double expected = dps * (1 * (1.0 + (TestWarrior.BasePrimaryAttributes[AttributeType.Strength].Amount / 100.0)));
      double margin = 0.00005;

      //Act
      double actual = TestWarrior.CalculateDamage();


      //Assert
      Assert.True(Math.Abs(actual - expected) < margin);
    }

    [Fact]
    public void CalculateDamage_Level1WarriorWithValid5DPSAxeAnd5StrengthArmorEquipped_5DPSWith10PercentBonus()
    {
      //Arrange
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);
      Weapon Axe = ItemFactory.MakeWeapon(attackspeed: 1, damage: 5, type: WeaponType.Axe);
      Armor PlateArmor = ItemFactory.MakeArmor(type: ArmorType.Plate, itemSlot:ItemSlot.Legs,
        bonusAttributes: new(){{ AttributeType.Strength, new Strength(5) }});

      TestWarrior.Equip(Axe);
      TestWarrior.Equip(PlateArmor);
      double dps = Axe.AttacksPerSecound * Axe.Damage;
      double expected = dps * (1 * (1.0 + (TestWarrior.TotalPrimaryAttributes[AttributeType.Strength].Amount / 100.0)));
      double margin = 0.00005;

      //Act
      double actual = TestWarrior.CalculateDamage();


      //Assert
      Assert.True(Math.Abs(actual - expected) < margin);
    }

    [Fact]
    public void Equip_5StrWarriorEquips3StrLegalArmorTwice_8Str()
    {
      //Arrange
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);
      Armor PlateArmor = ItemFactory.MakeArmor(type: ArmorType.Plate, itemSlot: ItemSlot.Legs,
        bonusAttributes: new() {{AttributeType.Strength, new Strength(3)}});
      int expected = 8;
      

      //Act
      TestWarrior.Equip(PlateArmor);
      TestWarrior.Equip(PlateArmor);
      int actual = TestWarrior.TotalPrimaryAttributes[AttributeType.Strength].Amount;

      //Assert
      Assert.Equal(expected, actual);

    }

    [Fact]
    public void CanEquip_WarriorAsksIfCanEquipClothArmor_False()
    {
      //Arrenge (including expected)
      Armor ClothArmor = ItemFactory.MakeArmor(type: ArmorType.Cloth);
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);

      //Assert && Act
      Assert.False(TestWarrior.CanEquip(ClothArmor));
    }

    [Fact]
    public void CanEquip_WarriorAsksIfCanEquipPlateArmor_True()
    {
      //Arrenge (including expected)
      Armor PlateArmor = ItemFactory.MakeArmor(type: ArmorType.Plate);
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);

      //Assert && Act
      Assert.True(TestWarrior.CanEquip(PlateArmor));
    }

    [Fact]
    public void CanEquip_WarriorAsksIfCanEquipDagger_False()
    {
      //Arrenge (including expected)
      Weapon dagger = ItemFactory.MakeWeapon(type: WeaponType.Dagger);
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);

      //Assert && Act
      Assert.False(TestWarrior.CanEquip(dagger));
    }

    [Fact]
    public void CanEquip_WarriorAsksIfCanEquipAxe_True()
    {
      //Arrenge (including expected)
      Weapon axe = ItemFactory.MakeWeapon(type: WeaponType.Axe); 
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);

      //Assert && Act
      Assert.True(TestWarrior.CanEquip(axe));
    }

    [Fact]
    public void CanEquip_Lvl1WarriorAsksIfCanEquipRequiredLevel2Axe_False()
    {
      //Arrenge (including expected)
      Weapon axe = ItemFactory.MakeWeapon(type: WeaponType.Axe, requiredLevel: 2);
      Character TestWarrior = CharacterFactory.GetCharacter(CharacterType.Warrior);

      //Assert && Act
      Assert.False(TestWarrior.CanEquip(axe));
    }
  }
}
