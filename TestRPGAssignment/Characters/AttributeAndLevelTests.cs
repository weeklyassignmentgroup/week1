﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Characters;

namespace TestRPGAssignment.Characters
{
  public class AttributeAndLevelTests
  {

    [Fact]
    public void CharacterConstructor_JustCreatingDefault_1()
    {
      //Arrenge
      Mage TestMage = CharacterFactory.GetMage();
      int expected = 1;
      //Act
      //Assert
      Assert.Equal(TestMage.Level, expected);
      
    }

    [Fact]
    public void LevelUp_FromLevel1ToLevel2_LevelEquals2()
    {
      //Arrenge
      Mage TestMage = CharacterFactory.GetMage();
      TestMage.LevelUp();
      int expected = 2;
      //Act
      //Assert
      Assert.Equal(TestMage.Level, expected);
    }
    [Fact]
    public void CharacterConstructor_Level1MageHasDefaultAttributes_8Int1Str1Dex()
    {
      //Arrenge
      Mage TestMage = CharacterFactory.GetMage();
      int expectedStr = 1;
      int expectedInt = 8;
      int expectedDex = 1;
      //Act

      //Assert
      Assert.Equal(TestMage.BasePrimaryAttributes[AttributeType.Intelligence].Amount, expectedInt);
      Assert.Equal(TestMage.BasePrimaryAttributes[AttributeType.Dexterity].Amount, expectedDex);
      Assert.Equal(TestMage.BasePrimaryAttributes[AttributeType.Strength].Amount, expectedStr);

    }

    [Fact]
    public void CharacterConstructor_Level1WarriorHasDefaultAttributes_1Int5Str2Dex()
    {
      //Arrenge
      Warrior TestWarrior = CharacterFactory.GetWarrior();
      int expectedStr = 5;
      int expectedInt = 1;
      int expectedDex = 2;
      //Act

      //Assert
      Assert.Equal(TestWarrior.BasePrimaryAttributes[AttributeType.Intelligence].Amount, expectedInt);
      Assert.Equal(TestWarrior.BasePrimaryAttributes[AttributeType.Dexterity].Amount, expectedDex);
      Assert.Equal(TestWarrior.BasePrimaryAttributes[AttributeType.Strength].Amount, expectedStr);

    }

    [Fact]
    public void CharacterConstructor_Level1RogueHasDefaultAttributes_1Int2Str6Dex()
    {
      //Arrenge
      Rogue TestRogue = CharacterFactory.GetRogue();
      int expectedStr = 2;
      int expectedInt = 1;
      int expectedDex = 6;
      //Act

      //Assert
      Assert.Equal(TestRogue.BasePrimaryAttributes[AttributeType.Intelligence].Amount, expectedInt);
      Assert.Equal(TestRogue.BasePrimaryAttributes[AttributeType.Dexterity].Amount, expectedDex);
      Assert.Equal(TestRogue.BasePrimaryAttributes[AttributeType.Strength].Amount, expectedStr);

    }

    [Fact]
    public void CharacterConstructor_Level1RangerHasDefaultAttributes_1Int1Str7Dex()
    {
      //Arrenge
      Ranger TestRanger = CharacterFactory.GetRanger();
      int expectedStr = 1;
      int expectedInt = 1;
      int expectedDex = 7;
      //Act

      //Assert
      Assert.Equal(TestRanger.BasePrimaryAttributes[AttributeType.Intelligence].Amount, expectedInt);
      Assert.Equal(TestRanger.BasePrimaryAttributes[AttributeType.Dexterity].Amount, expectedDex);
      Assert.Equal(TestRanger.BasePrimaryAttributes[AttributeType.Strength].Amount, expectedStr);

    }

    [Fact]
    public void LevelUp_WarriorLevelsFrom1To2AndGainsAttributes_8Str4Dex2Int()
    {
      //Arrange
      Warrior TestWarrior = CharacterFactory.GetWarrior();
      int expectedStr = 8;
      int expectedDex = 4;
      int expectedInt = 2;
      //Act
      TestWarrior.LevelUp();

      //Assert
      Assert.Equal(TestWarrior.BasePrimaryAttributes[AttributeType.Intelligence].Amount, expectedInt);
      Assert.Equal(TestWarrior.BasePrimaryAttributes[AttributeType.Dexterity].Amount, expectedDex);
      Assert.Equal(TestWarrior.BasePrimaryAttributes[AttributeType.Strength].Amount, expectedStr);
    }


    [Fact]
    public void LevelUp_MageLevelsFrom1To2AndGainsAttributes_2Str2Dex13Int()
    {
      //Arrange
      Mage TestMage = CharacterFactory.GetMage();
      int expectedStr = 2;
      int expectedDex = 2;
      int expectedInt = 13;
      //Act
      TestMage.LevelUp();

      //Assert
      Assert.Equal(TestMage.BasePrimaryAttributes[AttributeType.Intelligence].Amount, expectedInt);
      Assert.Equal(TestMage.BasePrimaryAttributes[AttributeType.Dexterity].Amount, expectedDex);
      Assert.Equal(TestMage.BasePrimaryAttributes[AttributeType.Strength].Amount, expectedStr);
    }

    [Fact]
    public void LevelUp_RogueLevelsFrom1To2AndGainsAttributes_3Str10Dex2Int()
    {
      //Arrange
      Rogue TestRogue = CharacterFactory.GetRogue();
      int expectedStr = 3;
      int expectedDex = 10;
      int expectedInt = 2;
      //Act
      TestRogue.LevelUp();

      //Assert
      Assert.Equal(TestRogue.BasePrimaryAttributes[AttributeType.Intelligence].Amount, expectedInt);
      Assert.Equal(TestRogue.BasePrimaryAttributes[AttributeType.Dexterity].Amount, expectedDex);
      Assert.Equal(TestRogue.BasePrimaryAttributes[AttributeType.Strength].Amount, expectedStr);
    }


    [Fact]
    public void LevelUp_RangerLevelsFrom1To2AndGainsAttributes_2Str12Dex2Int()
    {
      //Arrange
      Ranger TestRanger = CharacterFactory.GetRanger();
      int expectedStr = 2;
      int expectedDex = 12;
      int expectedInt = 2;
      //Act
      TestRanger.LevelUp();

      //Assert
      Assert.Equal(TestRanger.BasePrimaryAttributes[AttributeType.Intelligence].Amount, expectedInt);
      Assert.Equal(TestRanger.BasePrimaryAttributes[AttributeType.Dexterity].Amount, expectedDex);
      Assert.Equal(TestRanger.BasePrimaryAttributes[AttributeType.Strength].Amount, expectedStr);
    }
  }
}

