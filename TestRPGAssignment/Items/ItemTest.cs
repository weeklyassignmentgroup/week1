﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Items;

namespace TestRPGAssignment.Items
{
  public class ItemTest
  {

    [Theory]
    [InlineData(0)]
    [InlineData(-2)]
    public void WeaponConstructor_NegativeOrZeroDamage_ThrowArgumentException(double damage)
    {
      //AAA
      Assert.Throws<ArgumentException>(() => ItemFactory.MakeWeapon(damage: damage));
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-2)]
    public void WeaponConstructor_NegativeOrZeroAttackSpeed_ThrowArgumentException(double attackspeed)
    {
      //AAA
      Assert.Throws<ArgumentException>(() => ItemFactory.MakeWeapon(attackspeed: attackspeed));
    }

    [Theory]
    [InlineData(ItemSlot.Weapon)]
    public void ArmorConstructor_IllegalItemSlot_ThrowArgumentException(ItemSlot itemSlot)
    {
      //AAA
      Assert.Throws<InvalidArmorException>(() => ItemFactory.MakeArmor(itemSlot: itemSlot));
    }

    [Fact]
    public void CalculateDps_2Damage3Speed_6()
    {
      //Arrange
      Weapon weapon = ItemFactory.MakeWeapon(damage: 2, attackspeed:3);
      double expected = 6.0;
      //Act
      double actual = weapon.CalculateDamagePerSecound();
      //Assert
      Assert.Equal(expected, actual); 
    }





  }
}
