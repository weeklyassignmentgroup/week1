﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Characters;
using Week1.Items;
using Week1.Monster;

namespace TestRPGAssignment.AdventureMap
{
  public class RoomTests
  {


    [Fact]
    public void AttackBoss_Attacks1HealthBossWithAbout9Damage_IsClearedTrue()
    {
      //Arrange
      Monster boss = new(health: 1);
      Room room = new Dungeon(boss);
      Character character = CharacterFactory.GetMage();
      Weapon weapon = ItemFactory.MakeWeapon(damage: 3, type: WeaponType.Staff, attackspeed: 3);
      character.Equip(weapon);
      //Act
      room.AttackBoss(character);
      bool actual = room.IsCleared;
      //Assert
      Assert.True(actual);

    }

    [Fact]
    public void AttackBoss_Lvl1MageAttacks1HealthBossWithAbout9Damage_LevelsUpTo2()
    {
      //Arrange
      Monster boss = new(health: 1);
      Room room = new Dungeon(boss);
      int expected = 2;
      Character character = CharacterFactory.GetMage();
      Weapon weapon = ItemFactory.MakeWeapon(damage: 3, type: WeaponType.Staff, attackspeed: 3);
      character.Equip(weapon);
      //Act
      room.AttackBoss(character);
      int actual = character.Level;
      //Assert
      Assert.Equal(expected, actual);

    }

    [Fact]
    public void IsCleared_AttacksBossWithLessThanItsHealth_false()
    {
      //Arrange
      Monster boss = new(health: 20);
      Room room = new Dungeon(boss);
      Character character = CharacterFactory.GetMage();
      Weapon weapon = ItemFactory.MakeWeapon(damage: 3, type: WeaponType.Staff, attackspeed: 3);
      character.Equip(weapon);
      //Act
      room.AttackBoss(character);
      bool actual = room.IsCleared;
      //Assert
      Assert.False(actual);

    }

    [Fact]
    public void IsCleared_AttacksBossTwiceAndKillsInSecoundHit_True()
    {
      //Arrange
      Monster boss = new(health: 20, damage: 0);
      Room room = new Dungeon(boss);
      Character character = CharacterFactory.GetMage();
      Weapon weapon = ItemFactory.MakeWeapon(damage: 5, type: WeaponType.Staff, attackspeed: 2);
      character.Equip(weapon);
      //Act
      room.AttackBoss(character);
      room.AttackBoss(character);
      bool actual = room.IsCleared;
      //Assert
      Assert.False(actual);

    }

    [Fact]
    public void AttackBoss_3DamageBossRetaliatesToCharacterWhenBossSurvivesHit_charactertakes3damage()
    {
      //Arrange
      Monster boss = new(health: 20, damage: 3);
      Room room = new Dungeon(boss);
      //All characters have 10 life to start with
      Character character = CharacterFactory.GetMage();
      Weapon weapon = ItemFactory.MakeWeapon(damage: 1, type: WeaponType.Staff, attackspeed: 3);
      character.Equip(weapon);
      double expected = character.Health - boss.Damage;

      //Act
      room.AttackBoss(character);
      double actual = character.Health;
      //Assert
      Assert.Equal(expected, actual);

    }

    [Fact]
    public void IsAlive_20DamageBossRetaliatesToCharacterWhenBossSurvivesHit_False()
    {
      //Arrange
      Monster boss = new(health: 20, damage: 3);
      Room room = new Dungeon(boss);
      //All characters have 10 life to start with
      Character character = CharacterFactory.GetMage();
      Weapon weapon = ItemFactory.MakeWeapon(damage: 1, type: WeaponType.Staff, attackspeed: 3);
      character.Equip(weapon);
      bool expected = character.IsAlive();

      //Act
      room.AttackBoss(character);
      bool actual = character.IsAlive();

      //Assert
      Assert.Equal(expected, actual);

    }




  }
}
