﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Week1.Monster;

namespace TestRPGAssignment.AdventureMap
{
  public class MonsterTest
  {

    [Fact]
    public void Attack_20DamageAliveMonster_20()
    {
      Monster monster = new Monster(health: 2);
      double expected = monster.Damage;
      
      double actual = monster.AttackDamageIfAlive();

      Assert.Equal(expected, actual);
    }

    [Fact]
    public void Attack_20DamageDeadMonster_0()
    {
      Monster monster = new Monster(health: 0);
      double expected = 0;

      double actual = monster.AttackDamageIfAlive();

      Assert.Equal(expected, actual);
    }

    [Fact]
    public void TakeDamage_20DamageTaken10HealthMonster_False()
    {
      Monster monster = new Monster(health: 10);
      double damageTaken = 20;
      
      
      bool actual = monster.TakeDamageReturnIsAlive(damageTaken);

      Assert.False(actual);
    }

    [Fact]
    public void TakeDamage_5DamageTaken10HealthMonster_True()
    {
      Monster monster = new Monster(health: 10);
      double damageTaken = 5;


      bool actual = monster.TakeDamageReturnIsAlive(damageTaken);

      Assert.True(actual);
    }



  }
}
